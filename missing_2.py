"""
Copyright 2016 Rolando Brondolin, Marco Arnaboldi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import subprocess
import sys

test_output = open("output_amazon_hybrid.txt", "w")

test_output.write("com_amazon hybrid 4\n")

we = subprocess.Popen(['./work_efficient_hybrid', 'amazon_new.edges', '768', '512'], env={'OMP_NUM_THREADS' : '4'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo3"


test_output.close()