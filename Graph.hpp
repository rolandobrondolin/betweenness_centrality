/*
Copyright 2016 Rolando Brondolin, Marco Arnaboldi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __GRAPH_H__
#define __GRAPH_H__


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <set>
#include <vector>
#include <cstdlib>
#include <boost/algorithm/string.hpp>
#include <boost/bimap.hpp>

class Graph
{

public:
	unsigned num_vertex;
	unsigned num_edge;

	unsigned *R;
	unsigned *C;
	unsigned *F;

	boost::bimap<unsigned,std::string> IDs;

	Graph()
	{

	}

	~Graph()
	{
		delete R;
		delete C;
		delete F;
	}

	void parse_edgelist(char *file);

	void print_R();

	void print_number_of_isolated_vertices();

	void print_CSR();
		
	void print_high_degree_vertices();

	void print_adjacency_list();

	void print_numerical_edge_file(char *outfile);

	void print_BC_scores(const std::vector<float> bc, char *outfile);



};

#endif // __GRAPH_H__